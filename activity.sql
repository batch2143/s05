1. SELECT customers.customerName FROM customers WHERE customers.country = "Philippines";

2. SELECT customers.contactLastName, customers.contactFirstName FROM customers WHERE customers.customerName = "La Rochelle Gifts";

3. SELECT products.productName, products.MSRP FROM products WHERE products.productName = "The Titanic";

4. SELECT employees.firstName, employees.lastName FROM employees WHERE employees.email = "jfirrelli@classicmodelcars.com";

5. SELECT customers.customerName FROM customers WHERE customers.state IS NULL;

6. SELECT employees.firstName, employees.lastName, employees.email FROM employees WHERE employees.lastName = "Patterson" AND employees.firstName = "Steve";

7. SELECT customers.customerName, customers.country, customers.creditLimit FROM customers WHERE customers.country != "USA" AND customers.creditLimit > 3000;

8. SELECT orders.customerNumber from orders WHERE orders.comments LIKE "%DHL%";

9. SELECT productlines.productLine from productlines WHERE productlines.textDescription LIKE "%state of the art%";

10. SELECT DISTINCT customers.country FROM customers;

11. SELECT DISTINCT orders.status FROM orders;

12. SELECT customers.customerName, customers.country FROM customers WHERE customers.country IN ("USA", "France", "Canada");

13. SELECT employees.firstName, employees.lastName, offices.city FROM offices
	JOIN employees ON offices.officeCode = employees.officeCode WHERE employees.officeCode = 5;

14. SELECT customers.customerName FROM employees
	JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber WHERE customers.salesRepEmployeeNumber = 1166;

15. SELECT products.productName, customers.customerName from products
		JOIN orderdetails ON products.productCode = orderdetails.productCode
		JOIN orders ON orderdetails.orderNumber = orders.customerNumber	
		JOIN customers ON orders.customerNumber = customers.customerNumber
		WHERE customers.customerName = "Baane Mini Imports";


16. SELECT products.productName, customers.customerName from products
		JOIN orderdetails ON products.productCode = orderdetails.productCode
		JOIN orders ON orderdetails.orderNumber = orders.customerNumber	
		JOIN customers ON orders.customerNumber = customers.customerNumber
		WHERE customers.customerName = "Baane Mini Imports";


17. SELECT products.productName, products.quantityInStock FROM products
	WHERE productline = "PLANES" AND quantityInStock < 1000;

18. SELECT customers.customerName FROM customers WHERE phone LIKE "+81%";


